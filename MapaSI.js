function initMap(){
    var mymap = L.map('visor').setView([4.633232548442532, -74.15247917175293], 13);
    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', { attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoibG9yZW5hMjIiLCJhIjoiY2tjd3I1ZzFrMGdxbjJ5cGRqcDVvaTJ0NyJ9.pjJcuNgGlsJI0s50hg793Q'
}).addTo(mymap);

var polygon = L.polygon([
    [4.646749352352316, -74.18569564819336],
    [4.64897361172607, -74.1826057434082],
    [4.652395535512188, -74.18037414550781],
    [4.654961967438161, -74.17694091796875], 
    [4.654619777055395, -74.17573928833008],
    [4.655988537587979, -74.1741943359375],
    [4.65735729545751, -74.17196273803711], 
    [4.65735729545751, -74.17076110839844], 
    [4.656501822101171, -74.16921615600586], 
    [4.6568440115685465, -74.16715621948242], 
    [4.659495974295515, -74.16664123535156], 
    [4.6598381623058795, -74.16612625122069], 
    [4.658640503540908, -74.16449546813963],     
    [4.658726050663224, -74.16380882263184],
    [4.659495974295515, -74.16312217712402],
    [4.662062380312384, -74.16312217712402],
    [4.663174486675407, -74.16260719299316], 
    [4.663174486675407, -74.16183471679688],
    [4.662490113737273, -74.15986061096191],  
    [4.662917846901737, -74.15857315063475], 
    [4.662062380312384, -74.15677070617676],
    [4.664286591277714, -74.15471076965332],
    [4.664543230551236, -74.15144920349121],  
    [4.662661207034317, -74.14775848388672], 
    [4.661805740132468, -74.14441108703613],
    [4.6630889400946, -74.14183616638184],
    [4.663003393503384, -74.14106369018555],  
    [4.6590682390483344, -74.13720130920409], 
    [4.65530415765452, -74.13514137268066],
    [4.650427931366535, -74.13273811340332],
    [4.649401353108439, -74.13162231445311],
    [4.649401353108439, -74.13050651550293],
    [4.6481181281823245, -74.12887573242188],
    [4.649401353108439, -74.12724494934082],
    [4.647947031348949, -74.12638664245605],
    [4.6480325797708275, -74.12544250488281],
    [4.645979414779896, -74.12424087524414],
    [4.64187306686342, -74.12346839904785],
    [4.640333180233088, -74.12269592285156],
    [4.637338946614495, -74.11806106567383],
    [4.636483448963273, -74.11934852600098],
    [4.629040575706999, -74.12252426147461],
    [4.609449190592439, -74.12930488586424],
    [4.59627389410228, -74.137544631958],
    [4.594990572973916, -74.13797378540039],
    [4.595503901702505, -74.14132118225098],
    [4.594990572973916, -74.14621353149414],
    [4.59576056592819, -74.15239334106445],
    [4.596616112679607, -74.15170669555664],
    [4.596701667298249, -74.15102005004881],
    [4.595931675360621, -74.14990425109863],
    [4.597386103877693, -74.15050506591797],
    [4.598754975064128, -74.15136337280273],
    [4.599011638118746, -74.15205001831055],
    [4.600722722785758, -74.15213584899902],
    [4.601236047384077, -74.15265083312987],
    [4.601150493310061, -74.15342330932617],
    [4.602861572836787, -74.1551399230957],
    [4.603203788248495, -74.15659904479979],
    [4.604144879782129, -74.15711402893066],
    [4.606369273003686, -74.16286468505858],
    [4.6044015408934404, -74.1672420501709],
    [4.6044015408934404, -74.168701171875],
    [4.603374895892652, -74.16930198669434],
    [4.6030326805632065, -74.17187690734863],
    [4.603631557281716, -74.17573928833008],
    [4.606198166079785, -74.17728424072266],
    [4.608251446449103, -74.17779922485352],
    [4.609449190592439, -74.17719841003418],
    [4.612015778387003, -74.17891502380371],
    [4.613042410908224, -74.18131828308105],
    [4.6151812238932095, -74.18234825134277],
    [4.616036747282752, -74.18415069580078],
    [4.615694538050684, -74.18509483337402],
    [4.6188599671494, -74.18526649475098],
    [4.619287726754987, -74.18346405029297],
    [4.61877441519731, -74.18251991271973],
    [4.619116622943715, -74.18131828308105],
    [4.6188599671494, -74.18054580688477],
    [4.6170633739887235, -74.17891502380371],
    [4.616207851836899, -74.17754173278809],
    [4.61543788101837, -74.17685508728027],
    [4.616122299564984, -74.17616844177246],
    [4.620228796979006, -74.17136192321777],
    [4.625704089861983, -74.17505264282227],
    [4.623993065381306, -74.17651176452637],
    [4.625361885296538, -74.1771125793457],
    [4.629040575706999, -74.17745590209961],
    [4.635884599991032, -74.18063163757324],
    [4.64589386610884, -74.18535232543945],
    [4.646749352352316, -74.18569564819336]
]).addTo(mymap);
polygon.bindPopup("¡La mejor localidad de Bogotá!");

var iconMarker = L.icon({
    iconUrl: 'Imagenes/imagenes_17736.png',
    iconSize: [30,30],
    iconAnchor: [30,30]
})
var iconMarkerPark = L.icon({
    iconUrl: 'Imagenes/imagenes_9770.png',
    iconSize: [30,30],
    iconAnchor: [40,60]
})
var iconMarkerBibl = L.icon({
    iconUrl: 'Imagenes/imagenes_8154.png',
    iconSize: [30,30],
    iconAnchor: [40,60]
})
var iconMarkerHospital = L.icon({
    iconUrl: 'Imagenes/imagenes_8092.png',
    iconSize: [30,30],
    iconAnchor: [40,60]
})

var markerMundoAventura = L.marker([4.621619012069653, -74.13546323776245], {icon: iconMarkerPark, title: "Parque de Diversiones Mundo Aventura"});
markerMundoAventura.addTo(mymap);
markerMundoAventura.bindPopup("").openPopup();

var markerCañizales = L.marker([4.6249769049627645, -74.1616415977478], {icon: iconMarkerPark, title: "Parque Metropolitano Cayetano Cañizales"});
markerCañizales.addTo(mymap);
markerCañizales.bindPopup("").openPopup();

var markerTimiza = L.marker([4.609074895764483, -74.15380954742432], {icon: iconMarkerPark, title: "Parque Timiza"});
markerTimiza.addTo(mymap);
markerTimiza.bindPopup("").openPopup();

var markerTecho = L.marker([4.623811268787338, -74.13525938987732], {icon: iconMarker, title: "Estadio Metropolitano de Techo"});
markerTecho.addTo(mymap);
markerTecho.bindPopup("").openPopup();

var markerBiblioteca = L.marker([4.6431563031551955, -74.15480732917784], {icon: iconMarkerBibl, title: "Biblioteca Pública Tintal Manuel Zapata Olivella"});
markerBiblioteca.addTo(mymap);
markerBiblioteca.bindPopup("").openPopup();

var markerBTimiza = L.marker([4.609606929068037, -74.15822446346283], {icon: iconMarkerBibl, title: "Biblioteca Pública Lago Timiza"});
markerBTimiza.addTo(mymap);
markerBTimiza.bindPopup("").openPopup();

var markerPlazaA = L.marker([4.618827885168569, -74.1353988647461], {icon: iconMarker, title: "Centro Comercial Plaza de las Américas"});
markerPlazaA.addTo(mymap);
markerPlazaA.bindPopup("").openPopup();

var markerTintal = L.marker([4.642557459842802, -74.15629863739014], {icon: iconMarker, title: "Centro Comercial Tintal Plaza"});
markerTintal.addTo(mymap);
markerTintal.bindPopup("").openPopup();

var markerHospital = L.marker([4.6163629153034575, -74.15339916944504], {icon: iconMarkerHospital, title: "Hospital de Kennedy"});
markerHospital.addTo(mymap);
markerHospital.bindPopup("").openPopup();

var markerAbastos = L.marker([4.630783669428236, -74.15932416915894], {icon: iconMarker, title: "Corabastos"});
markerAbastos.addTo(mymap);
markerAbastos.bindPopup("").openPopup();

var markerUniA = L.marker([4.6163629153034575, -74.15339916944504], {icon: iconMarker, title: "Universitaria Agustiniana - UNIAGUSTINIANA"});
markerUniA.addTo(mymap);
markerUniA.bindPopup("").openPopup();

var markerPortAmericas = L.marker([4.629559226741557, -74.17297661304472], {icon: iconMarker, title: "Portal de Las Américas"});
markerPortAmericas.addTo(mymap);
markerPortAmericas.bindPopup("").openPopup();

var markerVaca = L.marker([4.6286021281441645, -74.16192054748535], {icon: iconMarkerPark, title: "Humedal La Vaca"});
markerVaca.addTo(mymap);
markerVaca.bindPopup("").openPopup();

var markerMonBanderas = L.marker([4.63119003421828, -74.14823055267334], {icon: iconMarker, title: "Monumento Banderas"});
markerMonBanderas.addTo(mymap);
markerMonBanderas.bindPopup("").openPopup();

var markerPrimera = L.marker([4.616293404098496, -74.13642883300781], {icon: iconMarker, title: "Primera de Mayo"});
markerPrimera.addTo(mymap);
markerPrimera.bindPopup("").openPopup();

     

var polygon1 = L.polygon([
    [4.62566131430035, -74.13793087005615],
    [4.615908418840066, -74.13992643356323],
    [4.6088503187730785, -74.13020610809326],
    [4.615480657196851, -74.12745952606201],
    [4.617576686784654, -74.13763046264648],
    [4.625447436453401, -74.13694381713867],
    [4.62566131430035, -74.13793087005615]
]).addTo(mymap);
polygon1.bindPopup("Primera de Mayo");
}